This directory contains all the necessary files for running the CBCC-RDG3 algorithm (using CMAES as the component optimizer) for solving the CEC'2013 benchmark problems.

-----------
References:
-----------
Sun Y, Li X, Erst A, Omidvar, M N. Decomposition for Large-scale Optimization Problems with Overlapping Components. In 2019 IEEE Congress on Evolutionary Computation (CEC), pp. 326-333. IEEE, 2019. 
 
Sun Y, Kirley M, Halgamuge S K. A Recursive Decomposition Method for 
Large Scale Continuous Optimization[J]. IEEE Transactions on 
Evolutionary Computation, 22, no. 5 (2018): 647-661. 
https://ieeexplore.ieee.org/abstract/document/8122017

----------------------
Files and directories:
----------------------
 - benchmark  : this directory contains the CEC'2013 benchmark problems.
 - rdg3       : this directory contains the Recursive Differential Grouping 3 algorithm.
 - result     : this directory is used to store the experimental results: best solutions found in each run.
 - trace      : this directory is used to store the time series data: best solutions found in the evolutionary process.
 - run.m      : this file should be executed to start the CBCC-RDG3 algorithm.
 - cbcc.m     : this function implements the CBCC framework that uses CMA-ES to solve each component cooperatively.
 - cmaes.m    : this function is an implementation of the CMA-ES algorithm.
 - grouping.m : this function is used to read the decomposition results of RDG3. 
 - README     : the readme file.

--------
License:
--------
This program is to be used under the terms of the GNU General Public License 
(http://www.gnu.org/copyleft/gpl.html).
Author: Yuan SUN
e-mail: yuan.sun@rmit.edu.au OR suiyuanpku@gmail.com
Copyright notice: (c) 2019 Yuan SUN
