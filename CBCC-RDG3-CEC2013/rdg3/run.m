% Author: Yuan SUN
% Email:  yuan.sun@rmit.edu.au 
%         suiyuanpku@gmail.com
%
% ------------
% Description:
% ------------
% This file is the entry point for running the recursive differential 
% gropuing 3 algorithm for decomposing CEC'2013 benchmark problems.

clear;
for func = [1:15]
    func_num = func
    
     % set the dimensionality and upper and lower bounds of the search space
    if (ismember(func_num, [13,14]))
        D = 905;
        lb = -100;
        ub = 100;
    elseif (ismember(func_num, [1,4,7,8,11,12,15]))
        D = 1000;
        lb = -100;
        ub = 100;
    elseif (ismember(func_num, [2,5,9]))
        D=1000;
        lb = -5;
        ub = 5;
    else 
        D=1000;
        lb = -32;
        ub = 32;
    end

    opts.lbound  = lb;
    opts.ubound  = ub;
    opts.dim     = D;

    addpath('benchmark');
    addpath('benchmark/datafiles');
    global initial_flag;
    initial_flag = 0;

    [seps, nonseps, FEs] = RDG3('benchmark_func', func_num, opts);

    filename = sprintf('./results/F%02d', func_num);
    save (filename, 'seps', 'nonseps', 'FEs','-v7');
end

