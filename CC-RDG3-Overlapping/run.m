% Author: Dr. Yuan SUN
% email address: yuan.sun@rmit.edu.au OR suiyuanpku@gmail.com
%
% ------------
% Description:
% ------------
% This file is the entry point for the CMAESCC-RDG3 algorithm which is a 
% cooperative co-evolutionary CMA-ES based on the recursive differential
% grouping 3 algorithm for tackling large-scale black-box optimiation 
% problems with overlapping components.
%
% -----------
% References:
% -----------
% Sun Y, Li X, Erst A, Omidvar, M N. Decomposition for Large-scale 
% Optimization Problems with Overlapping Components. In IEEE Congress 
% on Evolutionary Computation, accepted in March 2019. 
% 
% Sun Y, Kirley M, Halgamuge S K. A Recursive Decomposition Method for 
% Large Scale Continuous Optimization[J]. IEEE Transactions on 
% Evolutionary Computation, 22, no. 5 (2018): 647-661. 
% https://ieeexplore.ieee.org/abstract/document/8122017
%
% --------
% License: 
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Yuan SUN
% e-mail: yuan.sun@rmit.edu.au OR suiyuanpku@gmail.com
% Copyright notice: (c) 2019 Yuan Sun


clear;
% set random seed
rand('state', sum(100*clock)); 
randn('state', sum(100*clock));
%warning('off' ,'Octave:divide-by-zero');

% number of independent runs
runs = 40;

% number of fitness evaluations
Max_FEs = 3e6;

% for the benchmark functions initialization
global initial_flag;

myfunc = [1:20];
addpath('benchmark');
addpath('benchmark/datafiles');

for func_num = myfunc
    % load the FEs used by RDG3 in the decomposition process
    decResults = sprintf('./rdg3/results/F%02d', func_num);
    load (decResults);
    FEs = Max_FEs - FEs;

    % set the dimensionality and upper and lower bounds of the search
    % spaces
    D = 1000 - (mod(func_num -1,10)+1)*19;
    Lbound = -100.*ones(1,D);
    Ubound = 100.*ones(1,D);

    VTRs = [];
    bestval = [];
    for runindex = 1:runs
    % trace the fitness
    fprintf(1, 'Function %02d, Run %02d\n', func_num, runindex);
    filename = sprintf('trace/tracef%02d_%02d.txt', func_num, runindex);
    [fid, message] = fopen(filename, 'w');

    initial_flag = 0;
    % call the cmaescc algorithm
    [val]  = cmaescc('benchmark_func', func_num, D, Lbound, Ubound, FEs, fid);
    bestval = [bestval; val];
    fclose(fid);
    end

    bestval = sort(bestval);
    % the best results of each independent run
    filename = sprintf('result/bestf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', bestval);
    fclose(fid);

    % mean
    filename = sprintf('result/meanf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', mean(bestval)); 
    fclose(fid);

    %median
    filename = sprintf('result/medianf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', median(bestval));
    fclose(fid);

    % std
    filename = sprintf('result/stdf%02d.txt', func_num);
    [fid, message] = fopen(filename, 'w');
    fprintf(fid, '%e\n', std(bestval));
    fclose(fid);
end
    

