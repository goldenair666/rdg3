% Author: Yuan SUN
% Email:  yuan.sun@rmit.edu.au 
%         suiyuanpku@gmail.com
%
% ------------
% Description:
% ------------
% This file is the entry point for running the recursive differential 
% gropuing 3 algorithm for decomposing overlapping problems.

clear;
for func = [1:20]
    func_num = func
    
    lb = -100;
    ub = 100;    
    D = 1000 - (mod(func_num-1,10)+1)*19;

    opts.lbound  = lb;
    opts.ubound  = ub;
    opts.dim     = D;

    addpath('benchmark');
    addpath('benchmark/datafiles');
    global initial_flag;
    initial_flag = 0;

    [seps, nonseps, FEs] = RDG3('benchmark_func', func_num, opts);

    filename = sprintf('./results/F%02d', func_num);
    save (filename, 'seps', 'nonseps', 'FEs','-v7');
end

